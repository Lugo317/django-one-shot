from django.urls import path
from todos.views import todo_list_list_view, show_todo_list_view

urlpatterns = [
    path("", todo_list_list_view, name="todo_list_list"),
    path("<int:id>", show_todo_list_view, name="todo_list_detail"),
]
