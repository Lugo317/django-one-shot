from django.shortcuts import render
from todos.models import TodoList, TodoItem
from django.shortcuts import get_object_or_404
# Create your views here.


def todo_list_list_view(request):
    todos = TodoList.objects.all()
    context = {
        "todo_list_list": todos
    }
    return render(request, "todos/list.html", context)


def show_todo_list_view(request, id):
    todo = get_object_or_404(TodoList, id=id)
    context = {
        "todo_object": todo
    }
    return render(request, "todos/detail.html", context)
